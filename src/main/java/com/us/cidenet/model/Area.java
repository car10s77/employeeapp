package com.us.cidenet.model;

public enum Area {
	Administracion,
	Financiera,
	Compras,
	Infraestructura,
	Operacion,
	Talento_humano,
	Servicios_varios
}
