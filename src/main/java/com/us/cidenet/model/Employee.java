package com.us.cidenet.model;

import java.util.Date;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.Pattern;

import org.hibernate.validator.constraints.Length;
import org.springframework.format.annotation.DateTimeFormat;

import lombok.Data;

@Entity
@Table(name = "Employees")
@Data
public class Employee {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private int id;

    @NotBlank(message = "Campo requerido")
    @Length(min = 2, max = 20, message = "Debe contener mas de 2 caracteres y menos de 20")
    @Pattern(regexp = "^[A-Z]*$", message = "Solo caracteres en mayuscula")
    private String surname;

    @NotBlank(message = "Campo requerido")
    @Length(min = 2, max = 20, message = "Debe contener mas de 2 caracteres y menos de 20")
    @Pattern(regexp = "^[A-Z]*$", message = "Solo caracteres en mayuscula")
    private String secondSurname;

    @NotBlank(message = "Campo requerido")
    @Length(min = 2, max = 20, message = "Debe contener mas de 2 caracteres y menos de 20")
    @Pattern(regexp = "^[A-Z]*$", message = "Solo caracteres en mayuscula")
    private String name;

    @Length(max = 50, message = "Debe contener menos de 50")
    @Pattern(regexp = "^[A-Z]*$", message = "Solo caracteres en mayuscula")
    private String othersName;

    private String workCountry;
    private IdType idType;
    private String idNumber;
    private String email;

    @DateTimeFormat(pattern = "yyyy-MM-dd")
    private Date entryDate;
    private Area area;
    private boolean status = true;
    @DateTimeFormat(pattern = "dd-MM-yyyy hh:mm:ss")
    private Date registerDate;

    public Employee() {
    }
}
