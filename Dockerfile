FROM adoptopenjdk:8-jre-hotspot
ARG JAR_FILE=target/*.jar
COPY ${JAR_FILE}  cidenet-0.0.1.jar
ENTRYPOINT ["java","-jar","cidenet-0.0.1.jar"]