package com.us.cidenet.service.impl;

import java.time.Duration;
import java.util.Date;
import java.util.List;
import java.util.Optional;

import com.us.cidenet.model.Employee;
import org.springframework.stereotype.Service;

import com.us.cidenet.repository.EmployeeRepository;
import com.us.cidenet.service.EmployeeService;

@Service
public class EmployeeServiceImpl implements EmployeeService {


    EmployeeRepository employeeRepository;

    public EmployeeServiceImpl(EmployeeRepository employeeRepository) {
        this.employeeRepository = employeeRepository;
    }

    @Override
    public List<Employee> getAllEmployees() {
        return employeeRepository.findAll();
    }

    @Override
    public Employee saveEmployee(Employee employee) {
        Employee employeeValidated = generateEmail(employee);
        employeeValidated.setEntryDate(getValidEntryDate(employee));
        employeeValidated.setRegisterDate(validateRegisterDate(employeeValidated.getRegisterDate()));
        return employeeRepository.save(employeeValidated);
    }


    private Date validateRegisterDate(Date registerDate) {
        if (registerDate == null) {
            return new Date();
        } else {
            return registerDate;
        }
    }

    private Date getValidEntryDate(Employee employee) {
        Date entryDate = employee.getEntryDate();
        if (null != entryDate) {
            long DAY_IN_MS = 24 * 60 * 60 * 1000;
            Date MONTH_AGO = new Date(System.currentTimeMillis() - (30 * DAY_IN_MS));
            Date now = new Date(System.currentTimeMillis());
            long difference = Duration.between(entryDate.toInstant(), MONTH_AGO.toInstant()).toDays();
            int future = now.compareTo(entryDate);
            if (difference > 0 || future < 0) {
                return null;
            }
        }
        return entryDate;
    }

    private Employee generateEmail(Employee employee) {
        List<Employee> employees = getAllEmployees();
        String dom = setDomain(employee.getWorkCountry());
        if (employee.getEmail() == null) {
            employee.setEmail(employee.getName() + "." + employee.getSurname() + dom);
            employees.forEach((e) -> {
                if (e.getName().equals(employee.getName()) && e.getSurname().equals(employee.getSurname())) {
                    employee.setEmail(employee.getName() + "." + employee.getSurname() +
                            "." + (e.getId() + 1) + dom);
                } else {
                    employee.setEmail(employee.getName() + "." + employee.getSurname() + dom);
                }
            });
        }
        return employee;
    }

    private String setDomain(String workCountry) {
        if (workCountry.equals("Colombia") || workCountry.equals("colombia") || workCountry.equals("COLOMBIA")) {
            return "@cidenet.com.co";
        } else if (workCountry.equals("Estados unidos") || workCountry.equals("estados unidos") || workCountry.equals("ESTADOS UNIDOS")) {
            return "@cidenet.com.us";
        } else {
            return null;
        }
    }

    @Override
    public Employee getEmployeeById(int id) {
        Optional<Employee> _dBEmployee = employeeRepository.findById(id);
        Employee employee = null;
        if (_dBEmployee.isPresent()) {
            employee = _dBEmployee.get();
        } else {
            throw new RuntimeException("No se encuentra empleado con id: " + id);
        }
        return employee;
    }

    @Override
    public void deleteEmployee(int id) {
        this.employeeRepository.deleteById(id);
    }

}
