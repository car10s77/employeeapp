package com.us.cidenet.service;

import java.util.List;

import com.us.cidenet.model.Employee;

public interface EmployeeService {

    List<Employee> getAllEmployees();

    Employee saveEmployee(Employee employee);

    Employee getEmployeeById(int id);

    void deleteEmployee(int id);
}
