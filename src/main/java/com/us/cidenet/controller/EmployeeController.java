package com.us.cidenet.controller;

import java.util.List;

import javax.validation.Valid;

import com.us.cidenet.model.Employee;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;

import com.us.cidenet.service.EmployeeService;

@Controller
public class EmployeeController {

	@Autowired
	private EmployeeService employeeService;
	
	public List<Employee> getEmployees(){
		List<Employee> employees = employeeService.getAllEmployees();
		return employees;
	}
	
	@GetMapping(path="/employees")
	public String getAllEmployees(Model model){
		model.addAttribute("employees", this.getEmployees());
		return "employees";
	 }
	
	@GetMapping("/employees/new")
	public String showEmployeeForm(Model model) {
		Employee employee = new Employee();
		model.addAttribute("employee", employee);
		return "employeeForm";
	}
	
	@PostMapping("/employees")
	public String saveEmployee(@Valid @ModelAttribute("employee") Employee employee, BindingResult validationResult) {
		if(validationResult.hasErrors()) {
			return "employeeForm";
		}
		else {
		employeeService.saveEmployee(employee);
		return "redirect:/employees";
		}
	}
	
	@GetMapping("/editForm/{id}")
	public String showEditForm(@PathVariable(value = "id") int id, Model model) {
		Employee employee = employeeService.getEmployeeById(id);
		
		model.addAttribute("employee", employee);
		return "editForm";
	}
	
	@GetMapping("/deleteEmployee/{id}")
	public String deleteEmployee(@PathVariable int id) {
		this.employeeService.deleteEmployee(id);
		return "redirect:/employees";
	}
}
