package com.us.cidenet.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import com.us.cidenet.model.Employee;

public interface EmployeeRepository extends JpaRepository<Employee, Integer> {
	
}
