package com.us.cidenet.service.impl;

import com.us.cidenet.model.Employee;
import com.us.cidenet.repository.EmployeeRepository;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.TestInstance;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.springframework.test.context.junit.jupiter.SpringExtension;

import java.time.Duration;
import java.time.Instant;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.Mockito.when;

@TestInstance(TestInstance.Lifecycle.PER_CLASS)
@ExtendWith(SpringExtension.class)
class EmployeeServiceImplTest {


    @InjectMocks
    EmployeeServiceImpl service;

    @Mock
    EmployeeRepository employeeRepository;


    Employee employee1 = new Employee();
    Employee employee2 = new Employee();
    Employee employee3 = new Employee();

    @BeforeAll
    void setUp() {

        employee1.setName("ARMANDO");
        employee1.setSurname("MENDOZA");
        employee1.setSecondSurname("GALARZA");
        employee1.setWorkCountry("colombia");

        employee2.setName("BEATRIZ");
        employee2.setSurname("PINZON");
        employee2.setSecondSurname("SOLANO");
        employee2.setWorkCountry("estados unidos");

        employee3.setName("ARMANDO");
        employee3.setSurname("MENDOZA");
        employee3.setSecondSurname("VALENCIA");
        employee3.setWorkCountry("colombia");
    }

    @AfterEach
    void tearDown() {
        employee1.setEntryDate(null);
        employee2.setEntryDate(null);
        employee3.setEntryDate(null);
    }

    @Test
    void saveEmployeeHappyPath() {

        String emailEsperado = "ARMANDO.MENDOZA@cidenet.com.co";
        List<Employee> all = new ArrayList<>();

        when(employeeRepository.findAll()).thenReturn(all);
        when(employeeRepository.save(employee1)).thenReturn(employee1);

        Employee response = service.saveEmployee(employee1);

        assertEquals(response.getName(), employee1.getName());
        assertEquals(response.getSurname(), employee1.getSurname());
        assertEquals(response.getEmail(), emailEsperado);
        assertEquals(response.getEntryDate(), null);
    }

    @Test
    void generateEmailWhenNameAndSurnameAreTheSame() {

        String email1 = "ARMANDO.MENDOZA@cidenet.com.co";
        String email2 = "ARMANDO.MENDOZA.1@cidenet.com.co";

        List<Employee> all = new ArrayList<>();
        employee1.setEmail(email1);
        all.add(employee1);
        when(employeeRepository.findAll()).thenReturn(all);

        when(employeeRepository.save(employee3)).thenReturn(employee3);
        Employee response2 = service.saveEmployee(employee3);
        int algo = 0;

        assertEquals(response2.getEmail(), email2);
        assertNotEquals(employee1.getEmail(), response2.getEmail());
    }

    @Test
    void modifyEntryDateWhenPastDate() {
        List<Employee> all = new ArrayList<>();

        when(employeeRepository.findAll()).thenReturn(all);
        when(employeeRepository.save(employee2)).thenReturn(employee2);

        // Se guarda empleado sin fecha de ingreso

        Employee response = service.saveEmployee(employee2);

        assertEquals(response.getEntryDate(), null);

        Date fechaInvalida = new Date(2022,6,29);

        employee2.setEntryDate(fechaInvalida);

        Employee response2 = service.saveEmployee(employee2);

        // La fecha de ingreso no cambia

        assertEquals(response2.getEntryDate(), null);
    }

    @Test
    void modifyEntryDateWhenFutureDate() {
        List<Employee> all = new ArrayList<>();

        when(employeeRepository.findAll()).thenReturn(all);
        when(employeeRepository.save(employee2)).thenReturn(employee2);

        // Se guarda empleado sin fecha de ingreso

        Employee response = service.saveEmployee(employee2);

        assertEquals(response.getEntryDate(), null);

        Date actual = new Date();
        Instant fechaInvalida = actual.toInstant().plus(Duration.ofDays(1));

        employee2.setEntryDate(Date.from(fechaInvalida));

        Employee response2 = service.saveEmployee(employee2);

        // La fecha de ingreso no cambia

        assertEquals(response2.getEntryDate(), null);
    }

    @Test
    void modifyDateHappyPath() {
        List<Employee> all = new ArrayList<>();

        when(employeeRepository.findAll()).thenReturn(all);
        when(employeeRepository.save(employee2)).thenReturn(employee2);

        // Se guarda empleado sin fecha de ingreso

        Employee response = service.saveEmployee(employee2);

        assertEquals(response.getEntryDate(), null);

        Date actual = new Date();

        employee2.setEntryDate(actual);

        Employee response2 = service.saveEmployee(employee2);

        // La fecha de ingreso cambia

        assertEquals(response2.getEntryDate(), actual);
    }
}