package com.us.cidenet.model;

public enum IdType {
	Cedula_ciudadania,
	Cedula_extranjeria,
	Pasaporte,
	Permiso_especial
}
