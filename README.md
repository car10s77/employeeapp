# Carlos Barrios Carvajal

A Spring Boot-MySql application to handle employees

## Setup
This section describes the needed command to run the backend.
```
docker build -t employees .
docker-compose up
```

## Environment version
* Spring Boot: 2.7.2
* MySql: 8.0
